# dialog-demo-vue

#### 实现一个弹出框，从页面点击一个按钮后弹出这个弹框。弹框中需要同时具备以下特征：
*  具有关闭和确认按钮；
*  弹框宽度为视窗宽度和高度两者最大值的一半, 高度不大于500px，水平垂直居中；
*  弹框中间展示一个固定头部的表格, 表格中每页上下滚动展示30条行高32px的数据，具有分页效果
*  对视窗的“打开、关闭、展示30条数据”三条逻辑添加test cases。


## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your tests
```
yarn unit
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

