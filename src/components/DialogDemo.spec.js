import 'babel-polyfill';
import { mount } from '@vue/test-utils';
import DialogDemo from './DialogDemo.vue';

describe('DialogDemo', () => {
  const wrapper = mount(DialogDemo);
  it('popup a dialog that includes a table', async () => {
    const openBtn = wrapper.findAll('.el-button').at(0) // 拿到触发弹窗显示的按钮

    expect(wrapper.vm.$data.dialogTableVisible).toBeFalsy() // 控制弹窗的变量初始值为false
    await openBtn.trigger('click').then(() => {
      expect(wrapper.vm.$data.dialogTableVisible).toBeTruthy() // 控制弹窗的变量值是否变为true
    }) // 触发按钮的单击事件
  });

  it('show 30 recodes in the table', async () => {
    const rows = wrapper.findAll('.el-table__row'); // 拿到所有的行
    expect(rows.length).toBe(30);
  });

  it('hide the dialog', async () => {
    // const dialog = wrapper.find('#dialog') // 拿到弹窗
    const closeBtn = wrapper.find('.el-dialog__headerbtn') // 拿到触发弹窗显示的按钮

    expect(wrapper.vm.$data.dialogTableVisible).toBeTruthy() // 控制弹窗的变量初始值为true
    closeBtn.trigger('click').then(() => {
      expect(wrapper.vm.$data.dialogTableVisible).toBeFalsy() // 控制弹窗的变量值是否变为false
    }) // 触发按钮的单击事件
  });

});
